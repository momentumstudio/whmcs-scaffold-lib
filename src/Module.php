<?php
namespace Scaffold;

class Module
{
    /** @var string */
    protected $file;

    /** @var Config */
    protected $config;

    public function __construct(string $file, Config $config)
    {
        $this->file = $file;
        $this->config = $config;
    }

    public static function register(string $file, Config $config): self
    {
        // Register config
        self::registerConfig($config);

        return new self($file, $config);
    }

    protected static function registerConfig(Config $config): void
    {
        $functionName = $config->getModuleFileName() . '_config';

        if (!function_exists($functionName)) {
            eval(sprintf(
                "function %s() { return %s; }",
                $functionName,
                json_encode(Config::getInstance())
            ));
        }

        if (!function_exists($functionName)) {
            throw new \RuntimeException('Could not register configuration');
        }
    }
}