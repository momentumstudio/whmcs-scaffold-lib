<?php

namespace Scaffold;

use Illuminate\Contracts\Support\Arrayable;

class ConfigField implements Arrayable
{
    /** @var string */
    protected $name;

    /** @var string */
    protected $description;

    /** @var string One of text, password, yesno, checkbox, dropdown, radio or textarea */
    protected $type;

    /** @var int */
    protected $size;

    /** @var mixed */
    protected $default;

    /** @var string[] */
    protected $options;

    /** @var int */
    protected $rows = 3;

    /** @var int */
    protected $cols = 50;

    public function __construct(
        $configArray = []
    )
    {
        $this->name = $configArray['FriendlyName'];
        $this->type = $configArray['Type'];

        if ($configArray['Size']) {
            $this->size = $configArray['Size'];
        }

        if ($configArray['Description']) {
            $this->description = $configArray['Description'];
        }

        if ($configArray['Default']) {
            $this->default = $configArray['Default'];
        }

        if ($configArray['Options']) {
            $this->options = explode(',', $configArray['Options']);
        }

        if ($configArray['Rows']) {
            $this->rows = explode(',', $configArray['Rows']);
        }

        if ($configArray['Cols']) {
            $this->cols = explode(',', $configArray['Cols']);
        }
    }

    public static function text(string $name, int $size, ?string $description, ?string $default)
    {
        return self::textual('text', $name, $size, $description, $default);
    }

    public static function password(string $name, int $size, ?string $description, ?string $default)
    {
        return self::textual('password', $name, $size, $description, $default);
    }

    public static function yesno(string $name, int $size, ?string $description, ?string $default)
    {
        return self::textual('yesno', $name, $size, $description, $default);
    }

    public static function dropdown(string $name, ?string $description, string $options, ?string $default)
    {
        return self::choice('yesno', $name, $options, $description, $default);
    }

    public static function radio(string $name, ?string $description, string $options, ?string $default)
    {
        return self::choice('yesno', $name, $options, $description, $default);
    }

    public static function textarea(string $name, ?string $description, int $rows, int $cols, ?string $default)
    {
        $configArray = [
            'FriendlyName' => $name,
            'Type' => 'textarea',
            'Rows' => $rows,
            'Cols' => $cols
        ];

        if ($description) {
            $configArray['Description'] = $description;
        }

        if ($default) {
            $configArray['Default'] = $default;
        }

        return new self($configArray);
    }

    public function getSystemName(): string
    {
        return str_slug($this->name);
    }

    /**
     * Get the field as an array.
     *
     * @return array
     */
    public function toArray()
    {
        $configArray = [
            'FriendlyName' => $this->name,
            'Type' => $this->type
        ];

        if (!is_null($this->description)) {
            $configArray['Description'] = $this->description;
        }

        switch($this->type) {
            case 'text':
            case 'password':
            case 'yesno':
                $configArray['Size'] = $this->size;
                break;

            case 'dropdown':
            case 'radio':
                $configArray['Options'] = implode(',', $this->options);
                break;

            case 'textarea':
                $configArray['Rows'] = $this->rows;
                $configArray['Cols'] = $this->cols;
                break;
        }

        if (!is_null($this->default)) {
            $configArray['Default'] = $this->default;
        }

        return $configArray;
    }

    protected static function textual(string $type, string $name, int $size, ?string $description, ?string $default)
    {
        $configArray = [
            'FriendlyName' => $name,
            'Type' => $type,
            'Size' => $size
        ];

        if ($description) {
            $configArray['Description'] = $description;
        }

        if ($default) {
            $configArray['Default'] = $default;
        }

        return new self($configArray);
    }

    protected static function choice(string $type, string $name, string $options, ?string $description, ?string $default)
    {
        $configArray = [
            'FriendlyName' => $name,
            'Type' => $type,
            'Options' => $options
        ];

        if ($description) {
            $configArray['Description'] = $description;
        }

        if ($default) {
            $configArray['Default'] = $default;
        }

        return new self($configArray);
    }
}