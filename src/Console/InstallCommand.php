<?php

namespace Scaffold\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

class InstallCommand extends Command
{
    /** @inheritDoc */
    protected function configure()
    {
        $this->setName("install")
            ->setDescription("Installs WHMCS Scaffold framework");
    }

    /** @inheritDoc */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        if (file_exists('module.php')) {
            $io->title('WHMCS Scaffold Installation');

            $moduleName = $io->ask("Module Name (lowercase, not shown to users)", "module", function ($value) {
                if (preg_match('/^[a-z0-9-]+$/', $value) !== 1) {
                    throw new \RuntimeException('Your module name must be a valid file / directory name.');
                }

                return $value;
            });
            $friendlyName = $io->ask("Friendly Name", "My Module");
            $description = $io->ask("Description");
            $author = $io->ask("Author", "Joe Bloggs");
            $version = $io->ask("Version", "0.1");
            $fields = [];

            $addFields = $io->askQuestion(
                new ConfirmationQuestion("Would you like to add some configuration fields to WHMCS admin?", false)
            );

            if ($addFields) {
                $count = 1;

                while ($addFields) {
                    $io->section('Field ' . $count);

                    $fieldName = $io->ask("Friendly Name", "My Field");
                    $fieldDescription = $io->ask("Description");
                    $fieldType = $io->askQuestion(
                        new ChoiceQuestion("Field Type", [
                            'text',
                            'password',
                            'yesno',
                            'checkbox',
                            'dropdown',
                            'radio',
                            'textarea'
                        ])
                    );

                    $fieldExtras = [];
                    switch ($fieldType) {
                        case 'text':
                        case 'password':
                        case 'yesno':
                            $fieldExtras['size'] = (int)$io->ask("Field Size", 25);
                            break;

                        case 'dropdown':
                        case 'radio':
                            $fieldExtras['options'] = $io->ask("Field Options (comma-separated)", "Option1,Option2");
                            break;

                        case 'textarea':
                            $fieldExtras['rows'] = $io->ask("Field Rows", 3);
                            $fieldExtras['cols'] = $io->ask("Field Cols", 50);
                            break;
                    }

                    $fieldDefault = $io->ask("Field Default");

                    $fields[] = $this->generateField(
                        $fieldType,
                        $fieldName,
                        $fieldDescription,
                        $fieldExtras,
                        $fieldDefault
                    );

                    $count++;

                    $addFields = $io->askQuestion(
                        new ConfirmationQuestion("Would you like to add another field?")
                    );
                }
            }

            $io->writeln("<info>Updating your module file...</info>");

            $io->progressStart();

            $data = file_get_contents('module.php');

            $data = preg_replace('/->setName\(".*"\)/', '->setName("' . $friendlyName . '")', $data);
            $io->progressAdvance();

            if ($description) {
                $data = preg_replace('/->setDescription\(".*"\)/', '->setDescription("' . $description . '")', $data);
            } else {
                $data = preg_replace('/->setDescription\(".*"\)/', '', $data);
            }
            $io->progressAdvance();
            $data = preg_replace('/->setAuthor\(".*"\)/', '->setAuthor("' . $author . '")', $data);
            $io->progressAdvance();
            $data = preg_replace('/->setVersion\(".*"\)/', '->setVersion("' . $version . '")', $data);
            $io->progressAdvance();

            if (count($fields) > 0) {
                $data = preg_replace(
                    '/\/\/ Add your admin area fields here.../',
                    implode('\n', $fields),
                    $data
                );
                $io->progressAdvance();
            }

            file_put_contents('module.php', $data);
            $io->progressAdvance();

            rename('module.php', $moduleName . '.php');
            $io->progressFinish();

            $io->success("`module.php` file has been configured successfully. Thanks for using WHMCS Scaffold!");

        } else {
            $io->error("`module.php` file not found! Seems like its already installed...");
        }
    }

    private function generateField($type, $name, $description, array $extras, $default)
    {
        switch($type) {
            case 'text':
            case 'password':
            case 'yesno':
                return sprintf(
                    'ConfigField::%s("%s", "%s", %d, "%s")',
                    $type,
                    $name,
                    $extras['size'],
                    $description,
                    $default ? $default : null
                );

            case 'dropdown':
            case 'radio':
                return sprintf(
                    'ConfigField::%s("%s", "%s", "%s", "%s")',
                    $type,
                    $name,
                    $description,
                    $extras['options'],
                    $default ? $default : null
                );

            case 'textarea':
                return sprintf(
                    'ConfigField::%s("%s", "%s", %d, %d, "%s")',
                    $type,
                    $name,
                    $description,
                    $extras['rows'],
                    $extras['cols'],
                    $default ? $default : null
                );
        }

        throw new InvalidArgumentException('Unknown type!');
    }
}