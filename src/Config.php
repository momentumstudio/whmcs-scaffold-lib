<?php

namespace Scaffold;

use Illuminate\Contracts\Support\Arrayable;

/**
 * Singleton Config class to manage configuration items.
 * @package Scaffold
 * @author James King <james@momentum.studio>
 */
class Config implements Arrayable
{
    /** @var string */
    protected $file;

    /** @var string */
    protected $name;

    /** @var string */
    protected $description;

    /** @var string */
    protected $version;

    /** @var string */
    protected $author;

    /** @var ConfigField[] */
    protected $fields = [];

    private static $INSTANCE;

    public static function getInstance(string $file = null): self
    {
        if (!self::$INSTANCE) {
            self::$INSTANCE = new self($file);
        }

        return self::$INSTANCE;
    }

    public function getModuleFileName(): string
    {
        $file = str_replace('.php', '', $this->file);
        $file = last(explode('/', $file));

        return $file;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Config
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): Config
    {
        $this->description = $description;

        return $this;
    }

    public function getVersion(): string
    {
        return $this->version;
    }

    public function setVersion(string $version): Config
    {
        $this->version = $version;

        return $this;
    }

    public function getAuthor(): string
    {
        return $this->author;
    }

    public function setAuthor(string $author): Config
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return ConfigField[]
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @param ConfigField[] $fields
     * 
     * @return Config
     */
    public function setFields(array $fields): Config
    {
        $this->fields = $fields;

        return $this;
    }

    /**
     * Get the config as an array.
     *
     * @return array
     */
    public function toArray()
    {
        $fields = [];

        foreach($this->fields as $field) {
            $fields[$field->getSystemName()] = $field->toArray();
        }

        return [
            'name' => $this->name,
            'description' => $this->description,
            'version' => $this->version,
            'author' => $this->author,
            'fields' => $this->fields
        ];
    }

    protected function __construct(string $file)
    {
        $this->file = $file;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }
}